# README #

The patched version of Struts 1.2.9

Fixed vulnerabilities:
* CVE-2014-0114
* CVE-2016-1181
* CVE-2016-1182
* CVE-2015-0899

List of all vulnerabilities of this version: 
https://nvd.nist.gov/vuln/search/results?adv_search=true&isCpeNameSearch=true&query=cpe%3A2.3%3Aa%3Aapache%3Astruts%3A1.1%3A*%3A*%3A*%3A*%3A*%3A*%3A*

### How do I get set up? ###

Run `download-dependencies` task and then `dist`.
